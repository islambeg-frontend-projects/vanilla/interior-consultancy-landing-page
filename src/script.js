const rootEl = document.querySelector(":root");
const rootStyles = getComputedStyle(rootEl);
const navEl = document.querySelector(".nav");
const navHamburgerEl = document.querySelector(".nav-toggle");
const mainEl = document.querySelector(".main");

const smallScreenThreshold = rootStyles.getPropertyValue(
  "--small-screen-threshold",
);
const navAppearanceDelay = +rootStyles
  .getPropertyValue("--nav-appearance-delay")
  .slice(0, -2);

const smallScreenMQ = window.matchMedia(
  `only screen and (max-width: ${smallScreenThreshold})`,
);

function makeElementInvisible(el) {
  el.classList.add("sr-only");
}

function makeElementVisible(el) {
  el.classList.remove("sr-only");
}

function makeElementInvisibleWithDelay(el, delay = navAppearanceDelay + 100) {
  window.setTimeout(() => {
    el.classList.add("sr-only");
  }, delay);
}

function showElement(el) {
  el.style.display = "block";
}

function hideElement(el) {
  el.style.display = "none";
}

function onSmallScreenMQChange() {
  navHamburgerEl.classList.remove("nav-toggle--active");
  if (smallScreenMQ.matches) {
    makeElementInvisible(navEl);
    navEl.style.opacity = 0;
    navEl.style.transform = "translateX(100%)";
    window.setTimeout(() => {
      navEl.style.transition = `opacity ${navAppearanceDelay}ms ease,
    transform ${navAppearanceDelay}ms ease
    `;
    }, 0);
    showElement(navHamburgerEl);
  } else {
    navEl.style.opacity = null;
    navEl.style.transform = null;
    navEl.style.transition = null;
    makeElementVisible(navEl);
    hideElement(navHamburgerEl);
  }
  makeElementVisible(mainEl);
}

function onHamburgerToggle() {
  const isNavOpen = navHamburgerEl.classList.toggle("nav-toggle--active");
  if (isNavOpen) {
    navEl.style.opacity = 1;
    navEl.style.transform = "translateX(0)";
    makeElementVisible(navEl);
    makeElementInvisibleWithDelay(mainEl);
  } else {
    navEl.style.opacity = 0;
    navEl.style.transform = "translateX(100%)";
    makeElementInvisible(navEl);
    makeElementVisible(mainEl);
  }
}

smallScreenMQ.addEventListener("change", onSmallScreenMQChange);
navHamburgerEl.addEventListener("click", onHamburgerToggle);
