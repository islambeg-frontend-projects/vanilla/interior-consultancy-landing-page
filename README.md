<h1 align="center">Interior Consultancy Landing Page</h1>

<div align="center">
   Solution for a challenge from  <a href="http://devchallenges.io" target="_blank">Devchallenges.io</a>.
</div>

<div align="center">
  <h3>
    <a href="https://islambeg-frontend-projects.gitlab.io/vanilla/interior-consultancy-landing-page">
      Demo
    </a>
    <span> | </span>
    <a href="https://gitlab.com/islambeg-frontend-projects/vanilla/interior-consultancy-landing-page">
      Solution
    </a>
    <span> | </span>
    <a href="https://devchallenges.io/challenges/Jymh2b2FyebRTUljkNcb">
      Challenge
    </a>
  </h3>
</div>

## Table of Contents

- [Overview](#overview)
  - [Built With](#built-with)
- [Acknowledgements](#acknowledgements)
- [Contact](#contact)

## Overview

Simple landing page built with HTML and modern CSS.

### Built With

- HTML
- CSS
- [Sass](https://sass-lang.com/)

## Acknowledgements

- [A modern CSS Reset by Andy Bell](https://piccalil.li/blog/a-modern-css-reset/)
- [Favicon by Remix Design](https://remixicon.com/)
- [Arrow icon by Google](https://google.github.io/material-design-icons/)

## Contact

- Email islambeg@proton.me
- Gitlab [@islambeg](https://gitlab.com/islambeg)
